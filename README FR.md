# FindDuplicates

https://gitlab.com/pdea/FindDuplicates

Logiciel sous license GPL v3

https://www.gnu.org/licenses/quick-guide-gplv3.html


## Ce qu'il fait (résumé)

Vous aider à trouver et supprimer les doublons dans un ou plusieurs répertoires.
Un doublon consiste en 2 fichiers avec le même checksum (adler32sum) et la même taille, mais potentiellement un nom et/ou un chemin différents.

Aucun fichier n'est supprimé sans confirmation !

L'avantage de ce logiciel est de proposer des opérations de masse (filtrer, supprimer) sur les doublons trouvés pour vous aider à ranger votre disque, pas fichier par fichier.

## Comment le lancer

```
python FindDuplicatesQtGui.py
```

La version Python doit être python 3 avec PyQt installé (pas python 2)
Voir la configuration minimale ci-dessous

Ou avec des paramètres optionnels:
```
python FindDuplicatesQtGui.py -c ConfigFile.xml -d DirectoryToScan -s SelectionPattern -e ExcludePattern
```

## Petite histoire

Vous copiez parfois des fichiers d'un ordinateur à un autre ou sur une clé USB.
Vous effectuez également une copie des fichiers importants.
Mais au bout d'un moment, vous êtes un peu perdu, car vous avez beaucoup de doublons des mêmes fichiers
et il est difficile à trouver car vous pouvez modifier le nom de fichier ou l'organisation des fichiers dans les dossiers.
Et vous craignez de supprimer des fichiers parce que vous n'êtes pas sûr qu'il s'agit exactement du même fichier ou d'une version plus récente de ce fichier.
Alors ce petit logiciel est fait pour vous. :-)

## Ce qu'il fait (version longue)

Après avoir scanné le(s) répertoire(s) spécifié(s), ce logiciel affichera une interface pour vous permettre de choisir le fichier de chaque duplicata que vous souhaitez supprimer.

Vous pouvez trier les résultats par taille de fichier, chemin de fichier 1 ou 2 ou nom de fichier.
Le tri est inversé si vous cliquez deux fois sur le même bouton de tri.
Il est également possible de trier par sélection après avoir commencé à sélectionner les fichiers, afin d'avoir les fichiers sélectionnés au début.

Sélection: Vous pouvez sélectionner le fichier 1 ou 2.
Un clic sur la colonne 3 pour sélectionner le fichier 1, un clic sur la colonne 4 pour sélectionner le fichier 2.
En fonction de l'option "Autoriser la double sélection", vous pouvez sélectionner ou non les deux fichiers.

Vous pouvez sélectionner des fichiers par pattern (motif) dans FilePath ou dans FileName.
Vous pouvez sélectionner par anti-pattern (voir l'aide) dans Chemin.

Les fichiers sélectionnés peuvent être:
- exclus (non considérés comme des doublons),
- ou supprimé (avec confirmation)
- ou non sélectionné. ;-)

*** À NOTER ***
Si vous double-cliquez dans la première colonne sur un doublon, vous obtiendrez le chemin du répertoire 1 dans le presse-papiers.
Sur la deuxième colonne, vous obtiendrez le chemin complet du fichier 1.
Sur la 5ᵉ colonne, vous obtiendrez le chemin du répertoire 2.
Sur la 6ᵉ colonne, vous obtiendrez le chemin complet du fichier 2.
Ceci, pour ouvrir un répertoire dans l'explorateur ou un fichier avec un éditeur ou un outil externe.
Mais aussi pour vous aider à créer un motif spécial en le copiant dans la zone de saisie 'Pattern' pour rechercher et sélectionner des fichiers.

Ce logiciel vous apporte des fonctionnalités de fichier de configuration et de ligne de commande.

Consultez l'aide pour plus de détails.

## Configuration minimale

À partir des sources, vous avez besoin de python 3 pour lancer ce programme, mais aussi de la librairie PyQt5 python:
```
python.exe -m pip install --upgrade pip
    python.exe -m pip installer PyQt5
```

La version compilée de Windows est autonome.
Pour compiler le binaire Windows, vous avez également besoin de la bibliothèque cx_Freeze
```
    python.exe -m pip install cx_Freeze --upgrade
build python.exe Builder.py
    ou Build.bat
```

## Aide

- Double-cliquez sur la colonne FilePath 1 ou 2 (colonne 1 ou 5) copiera le chemin du répertoire dans le presse-papiers.
Double-cliquez sur la colonne fileName 1 ou 2 (colonne 2 ou 6) pour copier le chemin complet du fichier dans le presse-papiers.


- Lorsqu'il n'y a qu'un seul répertoire analysé, les chemins des fichiers sont affichés par rapport à ce répertoire de base (plus court).
Le chemin est vide si le fichier se trouve à la racine du répertoire d'analyse.
Vous pouvez redimensionner la largeur de la colonne de la liste.


- Exclure un doublon consiste à l'exclure de la liste des doublons, il ne sera donc plus considéré comme un doublon. Ainsi, il ne sera * PAS * supprimé.
Faites-le avec le répertoire contenant des copies de sauvegarde de vos fichiers.
Ou des copies que vous connaissez comme des doublons mais dont vous avez besoin sur les deux répertoires.
Exclure est l'exact opposé de supprimer!


- Pattern: un pattern est un répertoire ("/home/me/old/") ou une partie d'un répertoire ("/old /") ou d'un nom de fichier ("_svg", ".old", "_old. ").
Vous pouvez utiliser Pattern pour sélectionner (pattern) ou ne pas sélectionner (anti-pattern = sélectionner l'autre fichier en double paire).
Et ainsi pour exclure ou supprimer des fichiers.

Description du comportement de sélection anti-pattern (répertoire "sûr" par exemple).
Avec l'anti-pattern, le motif est le critère de * NON * sélection.
Si seul le chemin 1 correspond au modèle, sélectionne le fichier 2. Idem pour le chemin 2.
Si les deux correspondent au modèle, n'en sélectionne aucun.
Si aucun des chemins 1 et 2 ne correspond au modèle, n'en sélectionne aucun. Le motif doit être présent sur un fichier pour sélectionner l'autre fichier.
Exemple d'utilisation: Vous avez un répertoire que vous savez être celui que vous souhaitez conserver.
Tous les doublons ne figurant pas dans ce répertoire seront sélectionnés pour être supprimés (après vérification et confirmation bien sûr!).


- Autoriser la double sélection: activez cette option pour pouvoir sélectionner à la fois le fichier1 et le fichier2 d'un duplicata afin de pouvoir supprimer les deux en même temps.
ATTENTION: Cette option est alors dangereuse car elle permet de supprimer complètement un fichier et sa copie s'il n'y a qu'un seul doublon.


- Fichier de configuration
Lorsque vous ajoutez:
- un répertoire à scanner,
- ou un motif dans path ou fileName pour sélectionner,
- ou un motif dans le chemin à exclure,
un fichier de configuration est préparé pour vous.
Vous pouvez l'enregistrer si vous le souhaitez, pour la prochaine fois que vous souhaitez analyser le(s) même(s) répertoire(s).
Vous pourrez alors le charger.
Puisqu'il est enregistré en tant que fichier xml, vous pouvez le modifier avec un éditeur de texte.

Vous pouvez remplacer dans le fichier de configuration xml "PatternInFilePathToSelect" par "PatternInFileNameToSelect"
si vous souhaitez sélectionner les fichiers par motif dans leur nom plutôt que par leur chemin.

Vous pouvez également remplacer dans le fichier de configuration xml "PatternInFilePathToSelect" par "PatternToExclude"
si vous souhaitez exclure directement des fichiers sans l'étape de sélection à vérifier.


- Remarque: si 3 fichiers sont des doublons, ils seront affichés deux fois sur deux lignes distinctes.
Une ligne avec le fichier 1 et le fichier 2. Une autre ligne avec le fichier 2 et le fichier 3.
Si un motif détecte les fichiers 2 et 3 à sélectionner:
- lors de la comparaison des fichiers 2 et 3 (avec l'option "Autoriser la double sélection" désactivée), ils ne sont pas sélectionnés car les deux correspondent au motif,
- lors de la comparaison des fichiers 1 et 2, le fichier 2 est sélectionné.
Ainsi, lorsque des doublons sont affichés, le fichier 2 sera sélectionné sur les deux lignes car il s'agit du même fichier.
ATTENTION: Cela signifie que vous devez parfois rechercher et sélectionner à nouveau des doublons pour supprimer chaque doublon (Cf fichier3 ici).


- Différence entre "Scan directory" et "Add directory to scan":
"Scan directory" effacera les résultats précédents avant de scanner le nouveau répertoire.
"Ajouter un répertoire à analyser" ajoutera le résultat de l'analyse du répertoire au résultat de l'analyse précédente et trouvera les doublons sur le résultat total de l'analyse.


- ligne de commande :
```
python FindDuplicates -c ConfigFile.xml -d DirectoryToScan -s SelectionPattern -e ExcludePattern
```
Vous pouvez fournir plusieurs répertoires à analyser, plusieurs pattern de sélection et plusieurs pattern d'exclusion.
Vous ne devez pas utiliser à la fois -c et {-d, -s, -e}
python FindDuplicates -h affiche un message d'aide


- Si les doublons (uniquement les doublons) ont un chemin ou un nom de fichier avec des erreurs d'encodage,
ils seront affichés sur la sortie standard et sur un fichier "ErrFileEncoding.csv".
Ces fichiers seront automatiquement exclus de la liste des doublons.


## Remerciements

- Merci à Erwan L.P. de m'avoir convaincu d'apprendre Python et d'autres langages de programmation! ;-)
- Merci à Jérôme A.G. de m'avoir convaincu d'apprendre Perl aussi (ce qui m'a été utile dans le passé)! :-D

Développé par Florent PICHOT en 2017 et 2020-2021 (effet Covid!)
Merci de faire un don si ce logiciel vous a été utile, pour montrer votre gratitude. :-)


## Versions

- v2.0 10/04/2021 Interface graphique entièrement recréée avec PyQt (travail dur et ingrat!)
   Soyez gentil, c'est mon premier développement avec PyQt!
   Énorme amélioration des performances pour les répertoires avec beaucoup de fichiers
   Idées d'améliorations:
   Affichez les répertoires sans fichier pour supprimer les répertoires vides. Afficher les fichiers vides (taille = 0)

- v1.0 30/10/2017 Première version pour usage, mais aussi pour rapport de bogues, commentaires et proposition d'amélioration.
   Soyez gentil, c'est mon premier développement avec wxPython.
   Bug connu: les fenêtres contextuelles ne s'affichent pas correctement (grises, aucun message affiché) parfois la deuxième fois!?!
   Idées d'améliorations: Afficher la liste des fichiers avec les mêmes noms mais une taille / somme de contrôle différente (pour renommer un fichier et fusionner les répertoires)
