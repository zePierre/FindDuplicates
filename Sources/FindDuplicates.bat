@echo off

@rem ***************************
@rem Launch program from sources
@rem ***************************

@rem be sure to launch correct version of python (python 3 needed)
@rem set PythonPath=python.exe
@set PythonPath="c:\Program Files\Python39\python.exe"

@rem launch FindDuplicates GUI python script
@%PythonPath% FindDuplicatesQtGui.py